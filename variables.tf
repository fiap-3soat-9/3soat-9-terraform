variable "prefix" {}
variable "cluster_name" {}
variable "retention_days" {}
variable "desired_size" {}
variable "max_size" {}
variable "min_size" {}
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "product_database_secret" {}
variable "payment_database_secret" {}
variable "order_database_secret" {}

variable "private_subnets" {
  description = "List of private subnet IDs"
  type        = list(string)
}

variable "public_subnets" {
  description = "List of public subnet IDs"
  type        = list(string)
}
