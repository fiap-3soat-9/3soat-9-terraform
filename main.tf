module "vpc" {
  source = "./modules/vpc"
  prefix = var.prefix

  private_subnets = var.private_subnets
  public_subnets  = var.public_subnets
}

module "security_group" {
  source = "./modules/sg"
  vpc_id = module.vpc.vpc_id
}

module "eks" {
  prefix = var.prefix
  source = "./modules/eks"
  cluster_name = var.cluster_name

  vpc_id = module.vpc.vpc_id
  subnet_ids   = module.vpc.subnet_ids
  private_subnet_ids = module.vpc.private_subnet_ids
  public_subnet_ids = module.vpc.public_subnet_ids
  vpc_security_group_ids = module.security_group.security_group_ids

  retention_days = var.retention_days

  desired_size = var.desired_size
  max_size     = var.max_size
  min_size     = var.min_size
}

module "rds_product" {
  prefix = var.prefix
  source = "./modules/rds_product"

  vpc_id = module.vpc.vpc_id

  subnet_ids = module.vpc.private_subnet_ids
  vpc_security_group_ids = module.security_group.security_group_ids
  database_secret = var.product_database_secret
}

module "rds_payment" {
  prefix = var.prefix
  source = "./modules/rds_payment"

  vpc_id = module.vpc.vpc_id

  subnet_ids = module.vpc.private_subnet_ids
  vpc_security_group_ids = module.security_group.security_group_ids
  database_secret = var.payment_database_secret
}

module "rds_order" {
  prefix = var.prefix
  source = "./modules/rds_order"

  vpc_id = module.vpc.vpc_id

  subnet_ids = module.vpc.private_subnet_ids
  vpc_security_group_ids = module.security_group.security_group_ids
  database_secret = var.order_database_secret
}

module "dynamo_customer" {
  source = "./modules/dynamo_customer"
}
