# k8s-eks

Projeto responsável por criar um cluster EKS e VPC utilizando terraform

Para executar, basta utilizar os seguintes comandos:

`note: Necessário existir o bucket s3 - 3soat-9-hamburgueria para salvar o estado do terraform`

```
    terraform init
    terraform plan -out "planfile
    terraform apply -input=false ${TF_ROOT}/planfile
```
