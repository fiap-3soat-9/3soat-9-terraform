variable "prefix" {}
variable "vpc_id" {}
variable "subnet_ids" {}
variable "vpc_security_group_ids" {}
variable "database_secret" {}