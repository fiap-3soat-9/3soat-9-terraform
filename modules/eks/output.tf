#output "kubeconfig" {
#  description = "Kubeconfig para acesso ao cluster EKS."
#  value = module.eks.kubeconfig
#}

output "cluster_id" {
  description = "O ID do cluster EKS."
  value = module.eks.cluster_id
}