

module "eks" {
  source = "terraform-aws-modules/eks/aws"

  enable_cluster_creator_admin_permissions = true

  cluster_name    = "${var.prefix}-${var.cluster_name}"
  cluster_version = "1.29"

  cluster_endpoint_public_access = true
  cluster_endpoint_private_access = true

  vpc_id     = var.vpc_id
  subnet_ids = var.subnet_ids

  cluster_security_group_id = var.vpc_security_group_ids
  node_security_group_id = var.vpc_security_group_ids

  eks_managed_node_groups = {
    nodes = {
      min_size     = 1
      max_size     = 4
      desired_size = 2
      vpc_security_group_ids = [var.vpc_security_group_ids]
      capacity_type  = "SPOT"
      instance_types = ["t3.small"]
    }
  }

  tags = {
    Environment = "dev"
    Terraform   = "true"
  }
}

