module "dynamodb_table" {
  source   = "terraform-aws-modules/dynamodb-table/aws"

  billing_mode = "PROVISIONED"
  read_capacity = "20"
  write_capacity = "20"

  name     = "customer"
  hash_key = "document"

  attributes = [
    {
      name = "document"
      type = "S"
    }
  ]

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}