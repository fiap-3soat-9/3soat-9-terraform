module "rds" {
  source  = "terraform-aws-modules/rds/aws"
  version = "6.1.1"

  family = "postgres16"

  identifier = "${var.prefix}-payment"

  engine            = "postgres"
  engine_version    = "16.1"
  instance_class    = "db.t3.micro"
  allocated_storage = 5

  db_name     = "payment"
  username = "payment"
  password = var.database_secret

  vpc_security_group_ids = [var.vpc_security_group_ids, "sg-03259dd755a02a3aa"]
  create_db_subnet_group = true
  subnet_ids             = var.subnet_ids

  parameters = [
    {
      name  = "rds.force_ssl"
      value = "0"
    }
  ]

  publicly_accessible = true

  tags = {
    Environment = "dev"
    Terraform   = "true"
  }
}