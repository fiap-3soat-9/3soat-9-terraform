output "security_group_ids" {
  value       = aws_security_group.ssh_cluster.id
  description = "List of security group IDs created"
}