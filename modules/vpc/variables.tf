variable "prefix" {}

variable "private_subnets" {
 description = "List of private subnet IDs"
 type        = list(string)
}

variable "public_subnets" {
 description = "List of public subnet IDs"
 type        = list(string)
}
