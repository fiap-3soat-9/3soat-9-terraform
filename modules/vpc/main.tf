#resource "aws_vpc" "new-vpc" {
#  cidr_block = "10.0.0.0/16"
#  tags = {
#    Name = "${var.prefix}-vpc"
#  }
#  enable_dns_hostnames = true
#  enable_dns_support = true
#}

data "aws_availability_zones" "available" {}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  cidr = "10.0.0.0/16"

  azs = ["us-east-1a", "us-east-1b", "us-east-1c"]

  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  map_public_ip_on_launch = true  # Ensure this is set for public subnets

  enable_dns_hostnames = true
  enable_dns_support = true

  enable_nat_gateway   = true
  single_nat_gateway   = true
  one_nat_gateway_per_az = false

  tags = {
    "kubernetes.io/cluster/soat3gp9-hamburgueria-cluster" = "shared"
  }

  public_subnet_tags = {
    "kubernetes.io/cluster/soat3gp9-hamburgueria-cluster" = "shared"
    "kubernetes.io/role/elb"               = 1
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/soat3gp9-hamburgueria-cluster" = "shared"
    "kubernetes.io/role/internal-elb"      = 1
  }

}
