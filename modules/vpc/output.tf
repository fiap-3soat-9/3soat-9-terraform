output "vpc_id" {
  value = module.vpc.vpc_id
}

output "subnet_ids" {
  description = "IDs de todas as subnets criadas (públicas e privadas)"
  value = concat(module.vpc.public_subnets, module.vpc.private_subnets)
}

output "public_subnet_ids" {
  description = "IDs das subnets públicas"
  value = module.vpc.public_subnets
}

output "private_subnet_ids" {
  description = "IDs das subnets privadas"
  value = module.vpc.private_subnets
}